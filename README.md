I have developed this application using React JS.

To run this project, download the project from the given bitbucket url.

Run `npm install` to get the node_modules to run the project. Then you can run.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

